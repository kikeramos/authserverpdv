# nginx-express basic authentication

A basic authentication schema with nginx and a simple express server.
It Circunvents the user:pass basic url authentication, which is disallowed
in most of the modern browsers.

## Explanation and Usage

The nginx server redirects to the  (express) server for authentication.
The user and password is received in the query strings.
To test the idea the repo includes a one-page react-app. Run on root folder:

```shell
docker-compose up
```

The ngninx server listens in localhost and forwards to the express-server.
The credentials must be passed as `localhost/?user=userName&pass=p4sw00rD`. A cookie is sent back and any subdomain of localhost will be available up to the cookie's expiration.

## Environmental variables in docker-compose

```yml
- AUTH_PORT=3003   #(Port where the express server is listening)
- AUTH_PASSWORD=RW5yaXF1ZUppbWVuZXpSYW1vcw== #(mocks the user's password (btoa))
- AUTH_USER=enrique #(mocks the user name)
```

## Encoding Decoding

Method used to generate the decode of the password (and the corresponding decoder)

```js
//decoding
Buffer.from('EncodedPassword', 'base64').toString('base64') 

//encoding
Buffer.from('password', 'binary').toString('base64'))
```

### Author

Enrique Jimenez Ramos