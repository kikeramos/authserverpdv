const express = require('express');
const dotenv = require('dotenv');
const cookieParser = require("cookie-parser");

const fs = require('fs');

const app = express();

app.use(cookieParser());

// read .env and store in process.env
dotenv.config();

// config vars
const port = process.env.AUTH_PORT || 3000;
//const authUser = process.env.AUTH_USER || 'default'
//const authPassword = process.env.AUTH_PASSWORD || 'default';

//console.log(authPassword)
console.log(Buffer.from('EnriqueJimenezRamos', 'binary').toString('base64'))

// using single password for the time being, but this could query a database etc
const checkAuth = (user, pass) => {

  const data = fs.readFileSync('/credentials/credentials.json', 'utf8');
  // parse JSON string to JSON object
  const databases = JSON.parse(data);

  // print all databases
  return databases.some(db => {
    console.log(`${db.name}: ${db.pass}`);
    console.log(`${user}: ${pass}`);
    console.log(((user && pass) && (user === db.name && pass === db.pass)));
    return ((user && pass) && (user === db.name && pass === db.pass))
  });

};

// middleware to check auth status
const cookieVerify = (req, res, next) => {
  // get cookies

  const user = req.cookies['zipVision'];

  // check for missing tokens

  if (user) {

    console.log('cookieVerify ' + user)
    res.cookie('zipVision', user, { maxAge: 1000 * 60 * 1, httpOnly: true })
    req.status = true
  } else {
    req.status = false
  }

  return next();
};

app.use(cookieVerify);

// endpoint called by NGINX sub request
// expect cookie or user password
app.get('/auth', (req, res, next) => {
  // parameters from original client request
  // these could be used for validating request

  const requestUri = req.headers['x-original-uri'];
  const remoteAddr = req.headers['x-original-remote-addr'];
  const url_str = 'https://' + remoteAddr + requestUri
  const url = new URL(url_str)

  const user = url.searchParams.get('user')
  const pass = url.searchParams.get('pass')

  console.log({ 'req.status': req.status })
  if (req.status) {

    return res.sendStatus(200);
  }

  if (checkAuth(user, pass)) {

    console.log('check Auth')


    res.cookie('zipVision', user, { maxAge: 1000 * 60 * 1, httpOnly: true })

    return res.sendStatus(200);
  } else {
    console.log('fail')
    return res.sendStatus(401);
  }
});

// default 404
app.use((req, res, next) => {
  res.status(404).send('No such page');
});

app.listen(port, () => console.log(`Listening at: ${port}`));
